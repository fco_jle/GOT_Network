import pandas as pd
import urllib
from bs4 import BeautifulSoup
import urllib.request



# Conseguir una lista de todos los personajes de los libros: 
user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
url = "https://awoiaf.westeros.org/index.php/List_of_characters"
headers={'User-Agent':user_agent,} 

request=urllib.request.Request(url,None,headers) #The assembled request
response = urllib.request.urlopen(request)
data = response.read() # The data u need

soup = BeautifulSoup(data)

tags = soup("a")
links = []
for tag in tags:
    link = tag.get("href",None)
    links.append(link)
    print(tag.get("href",None))
    
character = links[58:2481]

got_characters = []
for char in character:
    name = char.split("/")
    if len(name)==3:
        got_characters.append(name[2])

#==============================================================================        
#==============================================================================
# Definir los nodos principales: Estos son los personajes "principales". 
main_nodes = ["Daenerys_Targaryen","Jon_Snow","Jaime_Lannister","Cersei_Lannister","Tyrion_Lannister","Sansa_Stark","Arya_Stark"]

# Definir funcion para extraer relaciones
def get_relations(name, limit):
    full_relations = {}
    char_name = name
    url = "https://awoiaf.westeros.org/index.php/"+name
    request=urllib.request.Request(url,None,headers) #The assembled request
    response = urllib.request.urlopen(request)
    data = response.read() # The data u need
    soup = BeautifulSoup(data)
    tags = soup("a")
    links = []
    for tag in tags:
        link = tag.get("href",None)
        links.append(link)
    rel_characters = []
    for char in links:
        if char is not None: 
            name = char.split("/")
            if len(name)==3:
                rel_characters.append(name[2])
    relateds = []
    for char in rel_characters:
        if char in got_characters:
            if char != "A_Game_of_Thrones":
                relateds.append(char)
    
    test_df = pd.DataFrame(data={"Source":char_name, "Target":relateds})
    t = test_df.groupby("Target").count().reset_index().sort_values(by="Source", ascending=False).head(limit)["Target"].tolist()
    
    full_relations[char_name] = t
    return full_relations

# Extraer relaciones
big_dict = {}
already_done = []

for char in main_nodes:
    already_done.append(char)
    dic = get_relations(char, 40)
    big_dict.update(dic)

subdict = {}
for key in big_dict.keys():
    chars_list = big_dict[key]
    for char in chars_list:
        if char not in already_done:
            already_done.append(char)
            dic = get_relations(char,30)
            subdict.update(dic)
            
subsubdic = {}
for key in subdict.keys():
    chars_list = subdict[key]
    for char in chars_list:
        if char not in already_done:
            already_done.append(char)
            dic = get_relations(char, 20)
            subsubdic.update(dic)

final = big_dict
final.update(subdict)
final.update(subsubdic)

nodes_list = []
for key in final.keys():
    for char in final[key]:
        nodes_list.append((key,char))
        
got_net = pd.DataFrame(columns=["Source","Target"], data=nodes_list)
got_net["Type"] = "Undirected"
got_net["Weight"] = 1
got_net = got_net.drop_duplicates()
got_net["Source"] = got_net["Source"].apply(lambda x: x.replace("_"," "))
got_net["Source"] = got_net["Source"].apply(lambda x: x.replace("%27","'"))
got_net["Target"] = got_net["Target"].apply(lambda x: x.replace("_"," "))
got_net["Target"] = got_net["Target"].apply(lambda x: x.replace("%27","'"))

got_net.to_csv("got_net.csv", index=False)


    

        
        
        
        
        